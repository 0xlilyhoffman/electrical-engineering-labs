# Electrical Engineering Labs

This section contains PDF files of exemplary lab write ups from the following courses:

* Control Systems Engineering 
* Electronics I 
* Electronics II

To download a PDF:

1. Navigate to "Source"
2. Select the desired file
3. Click "View raw". Download should start immediately.